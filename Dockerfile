FROM python:3.11
LABEL org.opencontainers.image.authors="Stéphane Ludwig <gitlab@stephane-ludwig.net>"

COPY requirements.txt .
RUN pip install -r requirements.txt --no-cache
